<?php

namespace App\Controller;

use garethp\ews\API;
use garethp\ews\API\Type;
use garethp\ews\MailAPI;

use App\Entity\Appointment;
use App\Form\AppointmentType;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class MainController extends AbstractController
{
    /**
     * @Route("/", name="main")
     */
    public function index(Request $request, MailerInterface $mailer): Response
    {
        date_default_timezone_set('Europe/Zurich');
        $data = $request->request->all();

        $app = new Appointment();
        $form = $this->createForm(AppointmentType::class, $app);

        $form->handleRequest($request);
        $success = false;
        if ($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($app);
            $em->flush();
            $email = (new Email())
                ->from('schochal@student.ethz.ch')
                ->to('schochal@student.ethz.ch')
                ->subject('[Agenda] New Meeting Request by ' . $app->getName())
                ->html('
                    <p>There\'s a new entry in the Agenda database: <a href="https://agenda.aschoch.ch/admin">https://agenda.aschoch.ch/admin</a></p>
                    <ul>
                        <li>Name: ' . $app->getName() . ' </li>
                        <li>Email: ' . $app->getEmail() . ' </li>
                        <li>Subject: [' . $app->getCategory() . '] '  . $app->getSubject() . ' </li>
                        <li>Place: ' . $app->getLocation() . '</li>
                        <li>Start: ' . $app->getStartAt()->setTimeZone(new \DateTimeZone("Europe/Zurich"))->format('d.M.Y H:i') . ' </li>
                        <li>End: ' . $app->getEndAt()->setTimeZone(new \DateTimeZone("Europe/Zurich"))->format('d.M.Y H:i') . ' </li>
                        <li>Notes: ' . $app->getNotes() . ' </li>
                    </ul>
                ');

            $mailer->send($email);
            $success = true;
        }

        //if(!count($data) == 0) {
        //    //date_default_timezone_set('CET');
        //    $app = new Appointment();
    
        //    $startAtStr = $data['startAt'];
        //    $startAt = \DateTimeImmutable::createFromFormat('j.m.Y, H:i:s', $startAtStr);
        //    $endAtStr = $data['endAt'];
        //    $endAt = \DateTimeImmutable::createFromFormat('j.m.Y, H:i:s', $endAtStr);
    
        //    $app->setSubject($data['subject']);
        //    $app->setLocation($data['location']);
        //    $app->setName($data['name']);
        //    $app->setEmail($data['email']);
        //    $app->setNotes($data['notes']);
        //    $app->setCategory($data['category']);
        //    $app->setStartAt($startAt);
        //    $app->setEndAt($endAt);
    
        //    $em = $this->getDoctrine()->getManager();
        //    $em->persist($app);
        //    $em->flush();
        //    $success = true;

        //    $email = (new Email())
        //        ->from('schochal@student.ethz.ch')
        //        ->to('schochal@student.ethz.ch')
        //        ->subject('[Agenda] New Meeting Request by ' . $app->getName())
        //        ->html('
        //            <p>There\'s a new entry in the Agenda database: <a href="https://agenda.aschoch.ch/admin">https://agenda.aschoch.ch/admin</a></p>
        //            <ul>
        //                <li>Name: ' . $app->getName() . ' </li>
        //                <li>Email: ' . $app->getEmail() . ' </li>
        //                <li>Subject: [' . $app->getCategory() . '] '  . $app->getSubject() . ' </li>
        //                <li>Place: ' . $app->getLocation() . '</li>
        //                <li>Start: ' . $app->getStartAt()->setTimeZone(new \DateTimeZone("Europe/Zurich"))->format('d.M.Y H:i') . ' </li>
        //                <li>End: ' . $app->getEndAt()->setTimeZone(new \DateTimeZone("Europe/Zurich"))->format('d.M.Y H:i') . ' </li>
        //                <li>Notes: ' . $app->getNotes() . ' </li>
        //            </ul>
        //        ');

        //    $mailer->send($email);
        //}

        return $this->render('main/index.html.twig', [
            'success' => $success,
            'form' => $form->createView(),
        ]);
    }
}
