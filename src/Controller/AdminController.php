<?php

namespace App\Controller;

use App\Entity\Appointment;
use App\Repository\AppointmentRepository;
use App\Form\EditAppointmentType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use garethp\ews\API;

class AdminController extends AbstractController
{
    public function __construct() {
        date_default_timezone_set('Europe/Zurich');
    }
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/index.html.twig', [
            'controller_name' => 'AdminController',
        ]);
    }

    /**
     * @Route("/admin/api", name="admin_api")
     */
    public function api(AppointmentRepository $ar): JsonResponse
    {
        $normalizers = [new DateTimeNormalizer(), new ObjectNormalizer()];
        $serializer = new Serializer($normalizers);

        $appointments = $ar->findAll();

        return new JsonResponse($serializer->normalize($appointments));
    }

    /**
     * @Route("/admin/approve/{id}", name="admin_approve")
     */
    public function approve(int $id, AppointmentRepository $ar): RedirectResponse
    {
        $app = $ar->findById($id)[0];
        $api = API::withUsernameAndPassword($_ENV['EWS_SERVER'], $_ENV['EWS_USERNAME'], $_ENV['EWS_PASSWORD'], ['timezone' => 'Central European Standard Time']);
        $calendar = $api->getCalendar();

        $start = $app->getStartAt()->setTimeZone(new \DateTimeZone('Europe/Zurich'));
        $end = $app->getEndAt()->setTimeZone(new \DateTimeZone('Europe/Zurich'));

        $timezones = $api->getServerTimezones();

        $createdItemIds = $calendar->createCalendarItems(array(
            'Subject' => $app->getSubject(),
            'Start' => $start->format('c'),
            'End' => $end->format('c'),
            'Location' => $app->getLocation() . ' {' . $app->getCategory() . '}',
            'RequiredAttendees' => array(
                array(
                    'Mailbox' => array(
                        'Name' => $app->getName(),
                        'EmailAddress' => $app->getEmail(), 
                    ) 
                ) 
            ),
        ), array('SendMeetingInvitations' => 'SendOnlyToAll'));
        return new RedirectResponse('/admin/remove/' . $app->getId());
    }

    /**
     * @Route("/admin/edit/{id}", name="admin_edit")
     */
    public function edit(int $id, AppointmentRepository $ar, Request $request): Response
    {
        $app = $ar->findById($id)[0];
        $form = $this->createForm(EditAppointmentType::class, $app);

        $form->handleRequest($request);

        if($form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return new RedirectResponse('/admin');
        }

        return $this->render('admin/edit/index.html.twig', [
            'form' => $form->createView(),     
            'title' => $app->getSubject(),
        ]);
    }

    /**
     * @Route("/admin/remove/{id}", name="admin_remove")
     */
    public function remove(int $id, AppointmentRepository $ar): RedirectResponse
    {
        $appointment = $ar->findById($id)[0];
        $em = $this->getDoctrine()->getManager();
        $em->remove($appointment);
        $em->flush();

        return new RedirectResponse('/admin');
    }
}
