<?php

namespace App\Controller;

use garethp\ews\API;
use garethp\ews\API\Type;
use garethp\ews\MailAPI;

use App\Entity\Appointment;
use App\Repository\AppointmentRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class ApiController extends AbstractController
{
    /**
     * @Route("/api", name="post")
     */
    public function index(AppointmentRepository $ar): JsonResponse
    {
        date_default_timezone_set('Europe/Zurich');
        $api = API::withUsernameAndPassword($_ENV['EWS_SERVER'], $_ENV['EWS_USERNAME'], $_ENV['EWS_PASSWORD']);
        $calendar = $api->getCalendar();
        $start = date('m') . '/' . '01/' . date('Y');
        if(date('m') != 12)
            $end = (date('m') + 1) . '/' . '01/' . date('Y');
        else
            $end = '01/' . '01/' . (date('Y') + 1);
        //$end = date('m') . '/' . cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y')) . '/' . date('Y');
        #$start = '09/01/2021';
        $start = date('m') . '/' . date('d') . '/' . date('Y');
        $end = '09/01/2023';

        $items = $calendar->getCalendarItems($start, $end);

        $entries = array();
        $index = 0;


        foreach($items as $item) {
            if(strpos($item->getLocation(), '{') !== false && strpos($item->getLocation(), '}') !== false) {
                $cat = explode('}', explode('{', $item->getLocation())[1])[0];
            } else {
                $cat = 'Unknown';
            }
            array_push(
                $entries,
                array(
                    'Id' => $index,
                    //'Subject' => $item->getSubject(),
                    'Subject' => $cat,
                    'StartTime' => $item->getStart(),
                    'EndTime' => $item->getEnd(),
                    'allDay' => $item->getIsAllDayEvent(),
                ),
            );
            $index++;
        }

        $tentative = $ar->findAll();

        foreach($tentative as $item) {
            array_push(
                $entries,
                array(
                    'Id' => $index,
                    'Subject' => 'Tentative',
                    'StartTime' => $item->getStartAt()->setTimeZone(new \DateTimeZone('UTC'))->format('Y-m-d\TH:i:s\Z'),
                    'EndTime' => $item->getEndAt()->setTimeZone(new \DateTimeZone('UTC'))->format('Y-m-d\TH:i:s\Z'),
                    //'StartTime' => $item->getStartAt()->format('Y-m-d\TH:i:s\Z'),
                    //'EndTime' => $item->getEndAt()->format('Y-m-d\TH:i:s\Z'),
                    'allDay' => false,
                )
            );
            $index++;
        }

        return new JsonResponse($entries);        
    }
}
