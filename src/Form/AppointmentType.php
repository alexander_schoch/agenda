<?php

namespace App\Form;

use App\Entity\Appointment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AppointmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Name', TextType::class, [
                'required' => True,
                'label' => false,
                'attr' => [
                  'placeholder' => 'Name*',
                ],
            ])
            ->add('Email', EmailType::class, [
                'label' => false, 
                'attr' => [
                  'placeholder' => 'E-Mail*',
                ],
                'required' => true,
            ])
            ->add('Subject', TextType::class, [
                'label' => false, 
                'attr' => [
                  'placeholder' => 'Subject*',
                ],
                'required' => true,
            ])
            ->add('Location', TextType::class, [
                'required' => false,
                'label' => false,
                'attr' => [
                  'placeholder' => 'Location',
                ],
            ])
            ->add('Category', ChoiceType::class, [
                'label' => false, 
                'attr' => [
                  'placeholder' => 'Category',
                ],
                'required' => true,
                'expanded' => false,
                'multiple' => false,
                'choices' => array( 
                    '-- Category --' => '-- Category --',    
                    'ETH' => 'ETH',
                    'VSETH' => 'VSETH',
                    'Work' => 'Work',
                    'Free Time' => 'Free Time',
                    'Private' => 'Private',
                    'Other' => 'Other',
                ),
                'choice_attr' => [
                    '-- Cateogry --' => ['data-color' => 'red'],
                ],
            ])
            ->add('StartAt', DateTimeType::class, [
                'required' => true, 
                'date_label' => 'asdf',
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
                'with_seconds' => false,
                'label' => 'Start At*',
                'input' => 'datetime_immutable',
            ])
            ->add('EndAt', DateTimeType::class, [
                'required' => true, 
                'date_label' => 'asdf',
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
                'with_seconds' => false,
                'label' => 'End At*',
                'input' => 'datetime_immutable',
            ])
            ->add('Notes')
            ->add('FurtherAttendees')
            ->add('Submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Appointment::class,
        ]);
    }
}
