<?php

namespace App\Form;

use App\Entity\Appointment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EditAppointmentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Name')
            ->add('Email')
            ->add('Subject')
            ->add('Category')
            ->add('Location')
            ->add('StartAt', DateTimeType::class, [
                'view_timezone' => 'Europe/Zurich', 
                'input' => 'datetime_immutable',
            ])
            ->add('EndAt', DateTimeType::class, [
                'view_timezone' => 'Europe/Zurich', 
                'input' => 'datetime_immutable',
            ])
            ->add('Notes')
            ->add('Update', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Appointment::class,
        ]);
    }
}
