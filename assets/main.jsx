import React from 'react';
import ReactDOM from 'react-dom';
import { Inject, ScheduleComponent, Day, Week, Month, Agenda, ViewsDirective, ViewDirective } from '@syncfusion/ej2-react-schedule';

class Calendar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
        entries: null,
    };
    //this.data = [{
    //  Id: 1,
    //  Subject: 'Meeting - 1',
    //  StartTime: new Date(2021, 7, 31, 10, 0),
    //  EndTime: new Date(2021, 7, 31, 12, 30),
    //  IsAllDay: false
    //}];
    this.getData = this.getData.bind(this);
    this.getData();
  }

  async getData() {
    const response = await fetch('/api');
    var data = await response.json();
    data.map(entry => {
      entry.StartTime = new Date(entry.StartTime);  
      entry.EndTime = new Date(entry.EndTime);  
    });
    this.setState({
        entries: data,
    });
  }
  
  render() {
    return (
      <div>
        <ScheduleComponent
          height='550px' 
          width='auto'
          currentView = 'Week'
          firstDayOfWeek = { 1 }
          eventSettings = { {dataSource: this.state.entries} }
          cssClass='schedule-cell-dimension'
        >
          <ViewsDirective>
            <ViewDirective option='Day' readonly={true}  />
            <ViewDirective option='Week' startHour='08:00' endHour='23:00' readonly={true}  />
            <ViewDirective option='Month' readonly={true} />
            <ViewDirective option='Agenda' readonly={true}  />
          </ViewsDirective>
          <Inject services={[Day, Week, Month, Agenda]} />
        </ScheduleComponent>
      </div>
    );
  }
}

ReactDOM.render(<Calendar/>, document.getElementById('react'));
