import React from 'react';
import ReactDOM from 'react-dom';
import { Alert, AlertTitle } from '@material-ui/lab';

class HowThisWorks extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div style={{ marginBottom: '20px' }}>
        <Alert severity="info">
          <AlertTitle>How this works</AlertTitle>
          <ol>
            <li>Look in my calendar for a time on which we are both available. If it is urgent, "Free Time" appointments can be rescheduled.</li>
            <li>Enter the meeting information</li>
            <li>Once I have approved of the meeting, you'll receive a calendar invitation via E-Mail </li>
          </ol>
        </Alert>
      </div>
    );
  }
}

ReactDOM.render(<HowThisWorks/>, document.getElementById('howthisworks'));
