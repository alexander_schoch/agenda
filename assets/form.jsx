import 'date-fns';
import React from 'react';
import ReactDOM from 'react-dom';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import TextField from '@material-ui/core/TextField';
import DateFnsUtils from '@date-io/date-fns';
import Button from '@mui/material/Button';
import {
    MuiPickersUtilsProvider,
    KeyboardTimePicker,
    KeyboardDatePicker,
} from '@material-ui/pickers';
import { alpha } from '@mui/material/styles';
import $ from 'jquery';
import Alert from '@mui/material/Alert';
import Snackbar from '@material-ui/core/Snackbar';
import axios from 'axios';

class Form extends React.Component {
  constructor(props) {
    var success = document.querySelector('#notifications').dataset.success;
    super(props);
    console.log(success);
    this.state = {
      success: success,
      error: false,
      name: '',
      email: '',
      subject: '',
      location: '',
      category: 'Free Time',
      startAt: new Date(),
      endAt: new Date(),
      notes: '',
    };
    this.categories = ['Work', 'ETH', 'VSETH', 'ExBeKo', 'TheAlt', 'Polyband', 'Free Time', 'Private', 'Other'];

    this.handleName = this.handleName.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.handleSubject = this.handleSubject.bind(this);
    this.handleLocation = this.handleLocation.bind(this);
    this.handleCategory = this.handleCategory.bind(this);
    this.handleStartAt = this.handleStartAt.bind(this);
    this.handleEndAt = this.handleEndAt.bind(this);
    this.handleNotes = this.handleNotes.bind(this);
    this.handleAttendees = this.handleAttendees.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.handleCloseS = this.handleCloseS.bind(this);
    this.handleCloseE = this.handleCloseE.bind(this);
  }

  handleCloseE(event, reason) {
    //if (reason === 'clickaway') {
      //return;
    //}

    let newState = Object.assign({}, this.state);
    newState.error = false;
    this.setState(newState);
  }

  handleCloseS(event, reason) {
    //if (reason === 'clickaway') {
      //return;
    //}

    let newState = Object.assign({}, this.state);
    newState.success = false;
    this.setState(newState);
  }

  handleSubject(event) {
    let newState = Object.assign({}, this.state);
    newState.subject = event.target.value;
    this.setState(newState);
  }

  handleLocation(event) {
    let newState = Object.assign({}, this.state);
    newState.location = event.target.value;
    this.setState(newState);
  }
  
  handleName(event) {
    let newState = Object.assign({}, this.state);
    newState.name = event.target.value;
    this.setState(newState);
  }

  handleEmail(event) {
    let newState = Object.assign({}, this.state);
    newState.email = event.target.value;
    this.setState(newState);
  }

  handleCategory(event) {
    let newState = Object.assign({}, this.state);
    newState.category = event.target.value;
    this.setState(newState);
  }

  handleStartAt(date) {
    let newState = Object.assign({}, this.state);
    newState.startAt = date;
    newState.endAt.setDate(newState.startAt.getDate()); 
    newState.endAt.setMonth(newState.startAt.getMonth()); 
    newState.endAt.setHours(newState.startAt.getHours() + 1);
    this.setState(newState);
  }

  handleEndAt(date) {
    let newState = Object.assign({}, this.state);
    newState.endAt = date;
    this.setState(newState);
  }

  handleNotes(event) {
    let newState = Object.assign({}, this.state);
    newState.notes = event.target.value;
    this.setState(newState);
  }

  handleAttendees(event) {
    let newState = Object.assign({}, this.state);
    newState.attendees = event.target.value;
    this.setState(newState);
  }


  onSubmit(e) {
    if(this.state.name != '' && this.state.email != '' && this.state.subject != '')
    {
      var form = $('<form action="" method="post">'
        + '<input name="name" value="' + this.state.name + '" />'
        + '<input name="email" value="' + this.state.email + '" />'
        + '<input name="subject" value="' + this.state.subject + '" />'
        + '<input name="location" value="' + this.state.location + '" />'
        + '<input name="category" value="' + this.state.category + '" />'
        + '<input name="startAt" value="' + this.state.startAt.toLocaleString('de-CH', {timeZone: 'CET'}) + '" />'
        + '<input name="endAt" value="' + this.state.endAt.toLocaleString('de-CH', {timeZone: 'CET'}) + '" />'
        + '<input name="notes" value="' + this.state.notes + '" />'
        + '</form>'
      );
      console.log(form);
      $('body').append(form);
      form.submit();
    } else {
      let newState = Object.assign({}, this.state);
      newState.error = true;
      this.setState(newState);
    }
  }
  
  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <div className="inputGroup">
            <TextField className="input" id="name" value={this.state.name} label="Name" onChange={this.handleName} required />
            <TextField className="input" id="email" value={this.state.email} label="E-Mail" onChange={this.handleEmail} required />
          </div>
          <div className="inputGroup">
            <TextField className="input" id="subject" value={this.state.subject} label="Subject" onChange={this.handleSubject} required />
            <TextField className="input" id="location" value={this.state.location} label="Location" onChange={this.handleLocation} />
          </div>
          <div className="inputGroup">
            <FormControl className="input"  style={{ minWidth: '120px' }}>
              <InputLabel id="label-category">Category</InputLabel>
              <Select
                labelId="label-category"
                id="category"
                value={this.state.category}
                onChange={this.handleCategory}
                required
              >
              {this.categories.map(category => {
                return <MenuItem key={category} value={category}>{category}</MenuItem>
              })}
              </Select>
            </FormControl>
          </div>
          <div className="inputGroup">
            <MuiPickersUtilsProvider className="input"  utils={DateFnsUtils}>
              <KeyboardDatePicker
                margin="normal"
                id="startAt"
                label="Start Date"
                format="dd. MMM. yyyy"
                value={this.state.startAt}
                onChange={this.handleStartAt}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
              <KeyboardTimePicker className="input" 
                margin="normal"
                id="startAtTime"
                label="Start Time"
                value={this.state.startAt}
                onChange={this.handleStartAt}
                KeyboardButtonProps={{
                  'aria-label': 'change time',
                }}
              />
            </MuiPickersUtilsProvider>
          </div>
          <div className="inputGroup">
            <MuiPickersUtilsProvider className="input"  utils={DateFnsUtils}>
              <KeyboardDatePicker
                margin="normal"
                id="endAt"
                label="End Date"
                format="dd. MMM. yyyy"
                value={this.state.endAt}
                onChange={this.handleEndAt}
                KeyboardButtonProps={{
                  'aria-label': 'change date',
                }}
              />
              <KeyboardTimePicker className="input" 
                margin="normal"
                id="endAtTime"
                label="End Time"
                value={this.state.endAt}
                onChange={this.handleEndAt}
                KeyboardButtonProps={{
                  'aria-label': 'change time',
                }}
              />
            </MuiPickersUtilsProvider>
          </div>
          <div className="inputGroup">
            <TextField className="input" id="notes" value={this.state.notes} label="Notes" onChange={this.handleNotes} multiline />
            <TextField className="input" id="attendees" value={this.state.attendees} label="Further Attendees" onChange={this.handleAttendees} multiline helperText='e.g. "maxmuster@gmx.ch, mariamuster@gmail.com"'/>
          </div>
          <div className="inputGroup">
            <Button variant="contained" color="primary" className="input" onClick={this.onSubmit}>
              Submit
            </Button>
          </div>
        </form>
        <Snackbar open={this.state.success} autoHideDuration={6000} onClose={this.handleCloseS}>
          <Alert severity="success" style={{ marginTop: '10px', marginBottom: '10px' }} onClose={this.handleClose}>Appointment sucessfully sent</Alert>
        </Snackbar>
        <Snackbar open={this.state.error} autoHideDuration={6000} onClose={this.handleCloseE}>
          <Alert severity="error" style={{ marginTop: '10px', marginBottom: '10px' }} onClose={this.handleClose}>Please enter all required fields</Alert>
        </Snackbar>
      </div>
    );
  }
}

ReactDOM.render(<Form/>, document.getElementById('form'));
