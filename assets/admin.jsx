import React from 'react';
import ReactDOM from 'react-dom';

import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import Typography from '@mui/material/Typography';
import Alert from '@mui/material/Alert';
import Button from '@mui/material/Button';

class Admin extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      entries: null,
    };
    this.getData = this.getData.bind(this);
    this.getData();
  }

  async getData() {
    const response = await fetch('/admin/api');
    var data = await response.json();
    data.map(entry => {
      entry.StartAt = new Date(entry.StartAt);
      entry.EndAt = new Date(entry.EndAt);
    });
    this.setState({
        entries: data,
    });
  }

  render() {
    if(this.state.entries == null || this.state.entries.length == 0) {
      return(
        <Alert severity="success">There are no entires in the database</Alert>
      );
    } else {
      return(
        <div>
          {this.state.entries.map(entry => (
            <Card key={entry.id} style={{ marginTop: '20px' }}>
              <CardContent>
                <Typography color="textSecondary" gutterBottom>
                  {entry.Name} (<a href="mailto:{entry.Email}">{entry.Email}</a>)
                </Typography>
                <Typography variant="h5" component="h2">
                  [{entry.Category}] {entry.Subject}
                </Typography>
                <Typography color="textPrimary">
                  @{entry.Location}
                </Typography>
                <Typography color="textSecondary">
                  Start: {entry.StartAt.toLocaleString('de-CH', {timeZone: 'CET'}).slice(0, -3)}
                </Typography>
                <Typography color="textSecondary">
                  End: {entry.EndAt.toLocaleString('de-CH', {timeZone: 'CET'}).slice(0, -3)}
                </Typography>
                <Typography variant="body2" component="p">
                  {entry.Notes}
                </Typography>
              </CardContent>
              <CardActions>
                <Button variant="contained" color="success" onClick={() => { window.location = "/admin/approve/" + entry.id }}>Approve</Button>
                <Button variant="contained" color="primary" onClick={() => { window.location = "/admin/edit/" + entry.id }}>Edit</Button>
                <Button variant="contained" color="error" onClick={() => { window.location = "/admin/remove/" + entry.id }}>Remove</Button>
              </CardActions>
            </Card>
            ))}
        </div>
      );
    }
  }
}

ReactDOM.render(<Admin/>, document.getElementById('admin'));
